
#' @export
pendientes <- function(db_con, poblacion, operacion = "CADR1C", start_date = "2019-03-01", end_date = "2018-09-02", materia = "") {

  
  operacion = rlang::enexpr(operacion)
  
  #start_date <- as.Date(end_date) - months(1) # comentamos la línea por Taiga 1849 
  #ultimo_dia_mesinf <- ymd(end_date) - 1
  resultado <- db_con %>% 
    apgyeTableData(!!operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>%
    group_by(iep) %>%
    filter(data_interval_start == max(data_interval_start)) %>% 
    collect()
  
  if (materia != "" & !operacion %in% c("CADR3C", "CADR3L")){
    resultado <- resultado %>%
      mutate(mat = toupper(mat)) %>% 
      filter(mat == materia)
  }
  
  if (nrow(resultado) == 0){
    resultado <- NULL
  } else {
      
    resultado <- resultado %>% 
      ungroup() %>% 
      filter(is.na(fres)) %>% 
      filter(!(is.na(nro) & is.na(caratula))) %>% # exclusión de registros con campos críticos vacíos
      mutate(as = toupper(as))
   
    if(str_detect(poblacion$organismo[[1]], "jdolab")) {
      resultado <- resultado %>%
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
               fecha_adespacho = fdesp, fecha_vencimiento = fvenc, año_mes, ultimo_dia_mesinf) %>% 
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho = dmy(fecha_adespacho), fecha_vencimiento = dmy(fecha_vencimiento)) %>%
        mutate(vencido = ifelse(fecha_vencimiento <= ultimo_dia_mesinf, "si", "no"))
      
    } else if(str_detect(poblacion$organismo[[1]], "camcco"))  {
      resultado <- resultado %>% 
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
               fecha_adespacho = fdesp1, fecha_vencimiento_1 = fvenc1, 
               fecha_vencimiento_2 = fvenc2, año_mes, ultimo_dia_mesinf,
               ordv) %>% #agregado de orv T2524
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento_2, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento_2)) %>% 
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho = dmy(fecha_adespacho),
               fecha_vencimiento_1 = dmy(fecha_vencimiento_1), fecha_vencimiento_2 = dmy(fecha_vencimiento_2)) %>%
        mutate(vencido_alvenc1 = ifelse(fecha_vencimiento_1 <= ultimo_dia_mesinf, "si", "no"),
               vencido_alvenc2 = ifelse(fecha_vencimiento_2 <= ultimo_dia_mesinf & 
                                          fecha_vencimiento_2 > fecha_vencimiento_1, "si", "no"))
      
    } else if(str_detect(poblacion$organismo[[1]], "camlab"))  {
      resultado <- resultado %>% 
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
               fecha_adespacho = fdesp1, fecha_vencimiento = fvenc, año_mes, ultimo_dia_mesinf) %>% 
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho = dmy(fecha_adespacho), fecha_vencimiento = dmy(fecha_vencimiento)) %>%
        mutate(vencido = ifelse(fecha_vencimiento <= ultimo_dia_mesinf, "si", "no"))
      
    } else if(str_detect(poblacion$organismo[[1]], "salcco"))  {
      resultado <- resultado %>% 
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
               fecha_adespacho1 = fdesp1, voc1, fecha_adespacho2 = fdesp2, voc2,
               fecha_adespacho3 = fdesp3, voc3, fecha_vencimiento1 = fvenc1, 
               fecha_vencimiento2 = fvenc2, año_mes, ultimo_dia_mesinf) %>% 
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento1, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento1)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho1 = dmy(fecha_adespacho1),
               fecha_adespacho2 = dmy(fecha_adespacho2), fecha_adespacho3 = dmy(fecha_adespacho3),
               fecha_vencimiento1 = dmy(fecha_vencimiento1)) %>%
        mutate(vencido = ifelse(fecha_vencimiento1 <= ultimo_dia_mesinf, "si", "no"))
      
    } else if(str_detect(poblacion$organismo[[1]], "sallab"))  {
      resultado <- resultado %>% 
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
               fecha_adespacho1 = fdesp1, voc1, fecha_adespacho2 = fdesp2, voc2,
               fecha_adespacho3 = fdesp3, voc3, fecha_vencimiento = fvenc, 
               año_mes, ultimo_dia_mesinf) %>% 
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho1 = dmy(fecha_adespacho1),
               fecha_adespacho2 = dmy(fecha_adespacho2), fecha_adespacho3 = dmy(fecha_adespacho3),
               fecha_vencimiento = dmy(fecha_vencimiento)) %>%
        mutate(vencido = ifelse(fecha_vencimiento <= ultimo_dia_mesinf, "si", "no"))
      
    } else {
      resultado <- resultado %>% 
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
               fecha_adespacho = fdesp, fecha_vencimiento_1 = fvenc1,
               fecha_vencimiento_2 = fvenc2, año_mes, ultimo_dia_mesinf) %>% 
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento_2, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento_2)) %>% 
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho = dmy(fecha_adespacho),
               fecha_vencimiento_1 = dmy(fecha_vencimiento_1), fecha_vencimiento_2 = dmy(fecha_vencimiento_2)) %>% 
        mutate(vencido_alvenc1 = ifelse(fecha_vencimiento_1 <= ultimo_dia_mesinf, "si", "no"),
               vencido_alvenc2 = ifelse(fecha_vencimiento_2 <= ultimo_dia_mesinf & 
                                          fecha_vencimiento_2 > fecha_vencimiento_1, "si", "no"))
    }
    
    resultado <- resultado  %>%   
      mutate(caratula = str_sub(word(caratula, 1,1), 1,15)) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo") %>% 
      select(-organismo) %>% 
      rename(organismo = organismo_descripcion) %>% 
      mutate(tproc = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tproc))) %>% 
      mutate(tipo_proc = str_sub(tproc, 1,20),
             organismo = abbreviate(organismo, minlength = 13)) %>% 
      select(-tproc, -ultimo_dia_mesinf)
    
    if(str_detect(poblacion$organismo[[1]], "jdolab")) {
      resultado <- resultado %>%   
        select(circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento, nro, caratula, tipo_proc, everything()) %>% 
        arrange(circunscripcion, organismo, desc(vencido))
    } else if(str_detect(poblacion$organismo[[1]], "camcco"))  {
      resultado <- resultado %>%   
        select(circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento_1, fecha_vencimiento_2, nro, caratula, tipo_proc, everything()) %>% 
        arrange(circunscripcion, organismo, desc(vencido_alvenc1))
    } else if(str_detect(poblacion$organismo[[1]], "camlab"))  {
      resultado <- resultado %>%   
        select(circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento, nro, caratula, tipo_proc, everything()) %>% 
        arrange(circunscripcion, organismo, desc(vencido))
    } else if(str_detect(poblacion$organismo[[1]], "salcco"))  {
      resultado <- resultado %>%   
        select(circunscripcion, organismo, año_mes, nro, fecha_adespacho1, voc1, fecha_adespacho2, voc2, 
               fecha_adespacho3, voc3, fecha_vencimiento1,
               fecha_vencimiento2, everything(),
               -caratula, -fecha_vencimiento2, -tipo_proc) %>% 
        arrange(circunscripcion, organismo, desc(vencido))
    } else if(str_detect(poblacion$organismo[[1]], "sallab"))  {
      resultado <- resultado %>%   
        select(circunscripcion, organismo, año_mes, nro, fecha_adespacho1, voc1, fecha_adespacho2, voc2, 
               fecha_adespacho3, voc3, fecha_vencimiento, everything(),
               -caratula, -tipo_proc) %>% 
        arrange(circunscripcion, organismo, desc(vencido))
    } else  {
      resultado <- resultado %>%   
        select(circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento_1, fecha_vencimiento_2, nro, caratula, tipo_proc, everything()) %>% 
        arrange(circunscripcion, organismo, desc(vencido_alvenc1))
    }
    
  }
  
  resultado
  
}
  
#' @export
f_pendientes_lic <- function(db_con, poblacion, operacion = "CADR1C", start_date = "2019-03-01", end_date = "2018-09-02", materia = "") {
  operacion = rlang::enexpr(operacion)
  resultado <- db_con %>%
    apgyeTableData(!!operacion) %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!poblacion$organismo) %>%
    group_by(iep) %>%
    filter(data_interval_start == max(data_interval_start)) %>%
    collect()

  if (materia != "" & !operacion %in% c("CADR3C", "CADR3L")){
    resultado <- resultado %>%
      mutate(mat = toupper(mat)) %>%
      filter(mat == materia)
  }

  if (nrow(resultado) == 0){
    resultado <- NULL
  } else {

    resultado <- resultado %>%
      ungroup() %>%
      filter(is.na(fres)) %>%
      filter(!(is.na(nro) & is.na(caratula))) %>% # exclusión de registros con campos críticos vacíos
      mutate(as = toupper(as))

    if(str_detect(poblacion$organismo[[1]], "jdolab")) {
      resultado <- resultado %>%
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio,
               fecha_adespacho = fdesp, fecha_vencimiento = fvenc, año_mes, ultimo_dia_mesinf, codigo_juez) %>%
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho = dmy(fecha_adespacho), fecha_vencimiento = dmy(fecha_vencimiento)) %>%
        mutate(vencido = ifelse(fecha_vencimiento <= ultimo_dia_mesinf, "si", "no")) %>%
        mutate(tiempo_pend = as.numeric(ultimo_dia_mesinf - fecha_adespacho))

    } else if(str_detect(poblacion$organismo[[1]], "camcco"))  {
      resultado <- resultado %>%
        mutate(
          año_mes = format(as.Date(data_interval_start), "%Y-%m"),
          ultimo_dia_mesinf = ymd(data_interval_end) - 1
        ) %>%
        select(
          organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio,
          fecha_adespacho = fdesp1, fecha_vencimiento_1 = fvenc1,
          fecha_vencimiento_2 = fvenc2, año_mes, ultimo_dia_mesinf,
          ordv, feg1, fdesp2, feg2, fdesp3, feg3
        ) %>%
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento_2, .keep_all = TRUE) %>%
        filter(is.na(feg3)) %>% #esto porque hay un caso que salió del 3er voto y falta fecha_adespacho
        mutate(
          fecha_adespacho = case_when(is.na(fecha_adespacho) ~ NA_Date_, TRUE ~ as.Date(dmy(fecha_adespacho))),
          fdesp2 = case_when(is.na(fdesp2) ~ NA_Date_, TRUE ~ as.Date(dmy(fdesp2))),
          fdesp3 = case_when(is.na(fdesp3) ~ NA_Date_, TRUE ~ as.Date(dmy(fdesp3))),
          feg1 = case_when(is.na(feg1) ~ NA_Date_, TRUE ~ as.Date(dmy(feg1))),
          feg2 = case_when(is.na(feg2) ~ NA_Date_, TRUE ~ as.Date(dmy(feg2))),
          feg3 = case_when(is.na(feg3) ~ NA_Date_, TRUE ~ as.Date(dmy(feg3))),
          fecha_vencimiento_1 = dmy(fecha_vencimiento_1),
          fecha_vencimiento_2 = dmy(fecha_vencimiento_2)
        ) %>%
        filter( #porque hay fdesp3 mayores al ultimo dia del mes
          (is.na(fecha_adespacho) | fecha_adespacho <= ultimo_dia_mesinf) &
            (is.na(fdesp2) | fdesp2 <= ultimo_dia_mesinf) &
            (is.na(fdesp3) | fdesp3 <= ultimo_dia_mesinf)
        ) %>%
        separate(ordv, into = c("Cam1", "Cam2", "Cam3"), sep = "\\s+|[[:punct:]]", remove = FALSE) %>%
        mutate(
          pend_en = case_when(
            !is.na(fecha_adespacho) & is.na(feg1) ~ "1",
            !is.na(fdesp2) & is.na(feg2) ~ "2",
            !is.na(fdesp3) & is.na(feg3) ~ "3",
            TRUE ~ "0"
          ),
          codigo_juez = case_when(
            pend_en == "1" ~ Cam1,
            pend_en == "2" ~ Cam2,
            pend_en == "3" ~ Cam3,
            TRUE ~ "0"
          )
        ) %>%
        filter(pend_en > 0) %>%
        mutate(
          tiempo_pend = case_when(
            pend_en == "1" ~ as.numeric(ultimo_dia_mesinf - fecha_adespacho, units = "days"),
            pend_en == "2" ~ as.numeric(ultimo_dia_mesinf - fdesp2, units = "days"),
            pend_en == "3" ~ as.numeric(ultimo_dia_mesinf - fdesp3, units = "days"),
            TRUE ~ NA_real_
          )
        )
    } else if(str_detect(poblacion$organismo[[1]], "camlab"))  {
      resultado <- resultado %>%
        mutate(
          año_mes = format(as.Date(data_interval_start), "%Y-%m"),
          ultimo_dia_mesinf = ymd(data_interval_end) - 1
        ) %>%
        select(
          organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio,
          fecha_adespacho = fdesp1, fecha_vencimiento = fvenc,
          año_mes, ultimo_dia_mesinf,
          ordv, feg1, fdesp2, feg2, fdesp3, feg3
        ) %>%
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
        filter(is.na(feg3)) %>% #esto porque hay un caso que salió del 3er voto y falta fecha_adespacho
        mutate(
          fecha_adespacho = case_when(is.na(fecha_adespacho) ~ NA_Date_, TRUE ~ as.Date(dmy(fecha_adespacho))),
          fdesp2 = case_when(is.na(fdesp2) ~ NA_Date_, TRUE ~ as.Date(dmy(fdesp2))),
          fdesp3 = case_when(is.na(fdesp3) ~ NA_Date_, TRUE ~ as.Date(dmy(fdesp3))),
          feg1 = case_when(is.na(feg1) ~ NA_Date_, TRUE ~ as.Date(dmy(feg1))),
          feg2 = case_when(is.na(feg2) ~ NA_Date_, TRUE ~ as.Date(dmy(feg2))),
          feg3 = case_when(is.na(feg3) ~ NA_Date_, TRUE ~ as.Date(dmy(feg3))),
          fecha_vencimiento = dmy(fecha_vencimiento)
        ) %>%
        filter( #porque hay fdesp3 mayores al ultimo dia del mes
          (is.na(fecha_adespacho) | fecha_adespacho <= ultimo_dia_mesinf) &
            (is.na(fdesp2) | fdesp2 <= ultimo_dia_mesinf) &
            (is.na(fdesp3) | fdesp3 <= ultimo_dia_mesinf)
        ) %>%
        separate(ordv, into = c("Cam1", "Cam2", "Cam3"), sep = "\\s+|[[:punct:]]", remove = FALSE) %>%
        mutate(
          pend_en = case_when(
            !is.na(fecha_adespacho) & is.na(feg1) ~ "1",
            !is.na(fdesp2) & is.na(feg2) ~ "2",
            !is.na(fdesp3) & is.na(feg3) ~ "3",
            TRUE ~ "0"
          ),
          codigo_juez = case_when(
            pend_en == "1" ~ Cam1,
            pend_en == "2" ~ Cam2,
            pend_en == "3" ~ Cam3,
            TRUE ~ "0"
          )
        ) %>%
        filter(pend_en > 0) %>%
        mutate(
          tiempo_pend = case_when(
            pend_en == "1" ~ as.numeric(ultimo_dia_mesinf - fecha_adespacho, units = "days"),
            pend_en == "2" ~ as.numeric(ultimo_dia_mesinf - fdesp2, units = "days"),
            pend_en == "3" ~ as.numeric(ultimo_dia_mesinf - fdesp3, units = "days"),
            TRUE ~ NA_real_
          )
        )
    } else if(str_detect(poblacion$organismo[[1]], "salcco"))  {
      resultado <- resultado %>%
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio,
               fecha_adespacho1 = fdesp1, voc1, fecha_adespacho2 = fdesp2, voc2,
               fecha_adespacho3 = fdesp3, voc3, fecha_vencimiento1 = fvenc1,
               fecha_vencimiento2 = fvenc2, año_mes, ultimo_dia_mesinf) %>%
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento1, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento1)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho1 = dmy(fecha_adespacho1),
               fecha_adespacho2 = dmy(fecha_adespacho2), fecha_adespacho3 = dmy(fecha_adespacho3),
               fecha_vencimiento1 = dmy(fecha_vencimiento1)) %>%
        mutate(vencido = ifelse(fecha_vencimiento1 <= ultimo_dia_mesinf, "si", "no"))

    } else if(str_detect(poblacion$organismo[[1]], "sallab"))  {
      resultado <- resultado %>%
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio,
               fecha_adespacho1 = fdesp1, voc1, fecha_adespacho2 = fdesp2, voc2,
               fecha_adespacho3 = fdesp3, voc3, fecha_vencimiento = fvenc,
               año_mes, ultimo_dia_mesinf) %>%
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho1 = dmy(fecha_adespacho1),
               fecha_adespacho2 = dmy(fecha_adespacho2), fecha_adespacho3 = dmy(fecha_adespacho3),
               fecha_vencimiento = dmy(fecha_vencimiento)) %>%
        mutate(vencido = ifelse(fecha_vencimiento <= ultimo_dia_mesinf, "si", "no"))

    } else {
      resultado <- resultado %>%
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
               ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
        select(organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio,
               fecha_adespacho = fdesp, fecha_vencimiento_1 = fvenc1,
               fecha_vencimiento_2 = fvenc2, año_mes, ultimo_dia_mesinf, codigo_juez) %>%
        distinct(organismo, nro, caratula, tproc, as, fecha_vencimiento_2, .keep_all = TRUE) %>%
        filter(!is.na(fecha_vencimiento_2)) %>%
        mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho = dmy(fecha_adespacho),
               fecha_vencimiento_1 = dmy(fecha_vencimiento_1), fecha_vencimiento_2 = dmy(fecha_vencimiento_2)) %>%
        mutate(vencido_alvenc1 = ifelse(fecha_vencimiento_1 <= ultimo_dia_mesinf, "si", "no"),
               vencido_alvenc2 = ifelse(fecha_vencimiento_2 <= ultimo_dia_mesinf &
                                          fecha_vencimiento_2 > fecha_vencimiento_1, "si", "no")) %>%
        filter(fecha_adespacho <= ultimo_dia_mesinf) %>% #porque hay fdesp mayores al ultimo dia del mes
        mutate(tiempo_pend = as.numeric(ultimo_dia_mesinf - fecha_adespacho))
    }

    resultado <- resultado  %>%
      mutate(caratula = str_sub(word(caratula, 1,1), 1,15)) %>%
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                                "circunscripcion")],
                by = "organismo") %>%
      select(-organismo) %>%
      rename(organismo = organismo_descripcion) %>%
      mutate(tproc = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tproc))) %>%
      mutate(tipo_proc = str_sub(tproc, 1,20)) %>%
      select(-tproc, -ultimo_dia_mesinf)

    if(str_detect(poblacion$organismo[[1]], "jdolab")) {
      resultado <- resultado %>%
        select(circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento,
               nro, caratula, tipo_proc, codigo_juez, tiempo_pend, everything()) %>%
        arrange(circunscripcion, organismo)
    } else if(str_detect(poblacion$organismo[[1]], "camcco"))  {
      resultado <- resultado %>%
        select(circunscripcion, organismo, año_mes, fecha_adespacho, feg1, fdesp2,
               feg2, fdesp3, feg3, nro, fecha_vencimiento_1, fecha_vencimiento_2,
               pend_en, caratula, tipo_proc, codigo_juez, tiempo_pend, everything()) %>%
        arrange(circunscripcion, organismo)
    } else if(str_detect(poblacion$organismo[[1]], "camlab"))  {
      resultado <- resultado %>%
        select(circunscripcion, organismo, año_mes, fecha_adespacho, feg1, fdesp2, feg2, fdesp3, feg3, nro, fecha_vencimiento,
               pend_en, caratula, tipo_proc, codigo_juez, tiempo_pend, everything()) %>%
        arrange(circunscripcion, organismo)
    } else if(str_detect(poblacion$organismo[[1]], "salcco"))  {
      resultado <- resultado %>%
        select(circunscripcion, organismo, año_mes, nro, fecha_adespacho1, voc1, fecha_adespacho2, voc2,
               fecha_adespacho3, voc3, fecha_vencimiento1,
               fecha_vencimiento2, codigo_juez, everything(),
               -caratula, -fecha_vencimiento2, -tipo_proc) %>%
        arrange(circunscripcion, organismo, desc(vencido))
    } else if(str_detect(poblacion$organismo[[1]], "sallab"))  {
      resultado <- resultado %>%
        select(circunscripcion, organismo, año_mes, nro, fecha_adespacho1, voc1, fecha_adespacho2, voc2,
               fecha_adespacho3, voc3, fecha_vencimiento, codigo_juez, everything(),
               -caratula, -tipo_proc) %>%
        arrange(circunscripcion, organismo, desc(vencido))
    } else  {
      resultado <- resultado %>%
        select(circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento_1,
               fecha_vencimiento_2, nro, caratula, tipo_proc, codigo_juez, everything()) %>%
        arrange(circunscripcion, organismo, desc(vencido_alvenc1), codigo_juez, tiempo_pend)
    }

  }

  resultado

}

#' @export
guarda_pendientes_lic <- function(pend){

  # pendientes_lic <- tbl(DB_PROD(), "pendientes_lic") %>% collect()
  #
  # pendientes_lic <- pendientes_lic %>%
  #   rows_upsert(pend, by = c("circunscripcion", "organismo", "año_mes", "codigo_juez")) %>%
  #   group_by(circunscripcion, organismo, codigo_juez) %>%
  #   filter(año_mes == max(año_mes))

  #copy_to(DB_PROD(), pendientes_lic, overwrite = TRUE, temporary = FALSE)
  pendientes_lic <- pend
  copy_to(DB_PROD(), pendientes_lic, overwrite = TRUE, temporary = FALSE)

}
