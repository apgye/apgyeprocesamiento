library(dplyr)
library(apgyeDSL)
library(stringr)

resultado <- DB_PROD() %>%
  apgyeTableData(CADR1C) %>%
  apgyeDSL::interval("2020-02-01", "2021-01-01") %>% collect()
  

resultado <- resultado %>% 
  filter(str_detect(mat, "P"),
         str_detect(toupper(justiciables), "IMPUT"))


# ----- inic proc constitucionales
db_con <- DB_PROD()
poblacion <- listar_organismos() %>% 
  filter(organismo == "stjpco0000pna")
start_date="2020-02-01"
end_date = "2021-03-01"

test <- inic_prim %>%
  mutate(año_mes = format(data_interval_start, "%Y-%m")) %>%
  codconver(operacion = "CIN3PC", variable = "tproc") %>% 
  group_by(circunscripcion, organismo, año_mes, tproc) %>%
  summarise(cantidad = n()) %>%
  pivot_wider(names_from = tproc, values_from = cantidad, values_fill = 0) %>% 
  select(everything(), "OTROS") %>% 
  group_by(circunscripcion, organismo) %>% 
  do(janitor::adorn_totals(.))
  
View(test)
  
# ----------------------------- caso civ 1 Colon, exclusion de casos
db_con <- DB_PROD()
poblacion <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(organismo == "jdocco0100col")
start_date="2018-02-01"
end_date = "2021-03-01"
operacion = "CADR1C"

un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)

operacion = rlang::enexpr(operacion)

resultado <- db_con %>%
  apgyeTableData(!! operacion) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) 

# colectamos para analizar
res_colectado <- resultado %>% collect()

colect_analizado <- res_colectado %>% 
  filter(!(iep == "jdocco0100col" & 
           (data_interval_start >= "2019-01-01" & data_interval_start <= "2020-01-01") &
           toupper(mat) %in% c("F", "L") &
           toupper(as) == "A"))

table(colect_analizado$data_interval_start)
table(colect_analizado$as)
table(colect_analizado$tres)
table(colect_analizado$mat)


colect_analizado_2 <- res_colectado %>% 
  filter(!(iep == "jdocco0100col" & 
             (data_interval_start >= "2019-01-01" &
             data_interval_start <= "2020-01-01") &
             toupper(mat) %in% c("F", "L") &
             tres == "10" & toupper(as) == "A"))


if(any("jdocco0100col" %in% poblacion$organismo)) {
  resultado <- resultado %>% 
    filter(!(iep == "jdocco0100col" & 
               data_interval_start >= "2019-01-01" &
               data_interval_start <= "2020-01-01"),
           !(iep == "jdocco0100col" & toupper(mat) %in% c("F", "L")), #exclusión por sobreregistración de remisiones por incompetencia
           !(iep == "jdocco0100col" & tres == "10" & toupper(as) == "A")) #y archivo de causas ante la creación de un Jdo Especializado.
}

if(desagregacion_mensual) {
  resultado <- resultado %>% group_by(iep, data_interval_start, data_interval_end)
} else {
  resultado <- resultado %>% group_by(iep)
}

resultado <- resultado %>%
  process() %>%
  .$result %>%
  ungroup()

#---- caso de Audiencias OMA
source("../apgyeinformes/R/informe.R")
db_con <- DB_PROD()
poblacion <- apgyeJusEROrganization::listar_organismos() %>%
  filter(tipo %in% c("oma", "equ"))
table(poblacion$organismo_descripcion)
start_date <- "2020-02-01"
end_date <- "2021-01-01"
