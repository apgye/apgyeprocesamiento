source("~/apgyeinformes/R/informe.R")
source("~/apgyeinformes/R/poblacion.R")
library(apgyeDSL)
library(apgyeJusEROrganization)
library(apgyeOperationsJusER)

bot <- Bot(token = R_TELEGRAM_BOT_RTelegramBot)
end_date = floor_date(Sys.Date(), "month")
start_date = floor_date(end_date %m-% months(12), "month")
ultimo_mes <- end_date - months(1)

res <- f_pendientes_lic(db_con = DB_PROD(), poblacion = jdos_cco,
                      operacion = "CADR1C",
                      start_date = start_date,
                      end_date = end_date)

resultado_res_civil <- res %>%
  group_by(circunscripcion, organismo, año_mes, codigo_juez) %>%
  filter(vencido_alvenc1=="si") %>% # estos son vencidos y pendientes
  summarise(cant=n(),
            causas = str_c(replace_na(nro, "S/nro"), collapse = ", "),
            dias_pend=str_c(tiempo_pend,collapse = ", "),
            prom_dias_pend = round(mean(as.numeric(tiempo_pend), na.rm = TRUE),0)) %>%
  select(circunscripcion, organismo, año_mes, codigo_juez, cant, prom_dias_pend, causas, dias_pend)



res_c2 <- f_pendientes_lic(db_con = DB_PROD(), poblacion = cam_civil,
                         operacion = "CADR2C",
                         start_date = start_date,
                         end_date = end_date)

resultado_res_camcivil <- res_c2 %>%
  group_by(circunscripcion, organismo, año_mes, codigo_juez) %>%
  summarise(cant=n(),
            causas = str_c(replace_na(nro, "S/nro"), collapse = ", "),
            dias_pend=str_c(tiempo_pend,collapse = ", "),
            prom_dias_pend = round(mean(as.numeric(tiempo_pend), na.rm = TRUE),0)) %>%
  select(circunscripcion, organismo, año_mes, codigo_juez, cant, prom_dias_pend, causas, dias_pend)



res_lab <- f_pendientes_lic(db_con = DB_PROD(), poblacion = jdos_lab,
                       operacion = "CADR1L",
                       start_date = start_date,
                       end_date = end_date)

resultado_res_lab <- res_lab %>%
  group_by(circunscripcion, organismo, año_mes, codigo_juez) %>%
  filter(vencido=="si") %>% # estos son vevncidos y pendientes
  summarise(cant=n(),
            causas = str_c(replace_na(nro, "S/nro"), collapse = ", "),
            dias_pend=str_c(tiempo_pend,collapse = ", "),
            prom_dias_pend = round(mean(as.numeric(tiempo_pend), na.rm = TRUE),0)) %>%
  select(circunscripcion, organismo, año_mes, codigo_juez, cant, prom_dias_pend, causas, dias_pend)


res_l2 <- f_pendientes_lic(db_con = DB_PROD(), poblacion = cam_lab,
                         operacion = "CADR2L",
                         start_date = start_date,
                         end_date = end_date)

resultado_res_camlab <- res_l2 %>%
  group_by(circunscripcion, organismo, año_mes, codigo_juez) %>%
  summarise(cant=n(),
            causas = str_c(replace_na(nro, "S/nro"), collapse = ", "),
            dias_pend=str_c(tiempo_pend, collapse = ", "),
            prom_dias_pend = round(mean(as.numeric(tiempo_pend), na.rm = TRUE),0)) %>%
  select(circunscripcion, organismo, año_mes, codigo_juez, cant, prom_dias_pend, causas, dias_pend)


pend4 <- bind_rows(resultado_res_civil,
                   resultado_res_camcivil,
                   resultado_res_lab,
                   resultado_res_camlab)

guarda_pendientes_lic(pend4)

jueces <- length(pend4 %>% ungroup() %>% filter(!(codigo_juez == "" | codigo_juez == "NA")) %>% .$codigo_juez)
causas <- sum(pend4$cant)
mediana <- median(pend4$prom_dias_pend)
promedio <- round(mean(pend4$prom_dias_pend))

texto <- str_c("Tema: Pendientes para Lic. \nSe guardó pend4, hasta ",ultimo_mes, " inclusive.",
               "\n\nAdemás dispone de un total de ", jueces,
               " magistrados identificados y un total de ",causas,
               " causas pendientes (Fuero Civ-Lab, Inst 1 y 2). ",
               "\n\nEn cuanto a los días pendientes, la mediana es ",mediana,
               " y el promedio es ",promedio,".")

# texto <- "nada"
bot$sendMessage( R_TELEGRAM_USER_marcos, text = texto)
bot$sendMessage( R_TELEGRAM_USER_stef, text = texto)

#write.table(pend4, file = "pendientes_para_sgp.xlsx", row.names = F, col.names = T)
